package cat.epiabalmes.damviod.tetris.Tetris;

import java.util.Random;

import cat.epiabalmes.damviod.tetris.framework.Game;
import cat.epiabalmes.damviod.tetris.framework.Graphics;
import cat.epiabalmes.damviod.tetris.framework.Pixmap;


@SuppressWarnings("ALL")
public class Board {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 16;
    static final int SCORE_INCREMENT = 10;

    static final float TICK_INITIAL = 0.5f;
    int current = 0;
    Game game;
    Peça peça;

    boolean canmove_der = false;
    boolean canmove_izq = false;
    public boolean gameOver = false;
    boolean linea = false;
    public int score = 0;
    boolean end = false;
    int filas = 0;
    int linia_borrar;
    int columnas = 0;
    boolean rotable = false;
    int fields[][] = new int[WORLD_WIDTH+5][WORLD_HEIGHT+5];
    boolean fet = false;
    float tickTime = 0;
    float tick = TICK_INITIAL;
    Pixmap currentPixmap;

    public Board(Game game) {
        Random rand = new Random();
        int n = rand.nextInt(6) + 1;
        peça = new Peça(n);
        this.game = game;


    }


    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        end = false;

        for (int c = 0; c < WORLD_HEIGHT; c++) {
            linea = true;
            for (int l = 0; l < WORLD_WIDTH; l++) {
                if (fields[l][c] == 0) {
                    linea = false;
                }


            }
            if (linea) {
                Borrar_Linea(c);

            }


        }


        if (peça.y + peça.h == WORLD_HEIGHT - 1) {

            for (int i = 0; i < peça.w; i++) {
                // filas++;
                for (int j = 0; j < peça.h; j++) {

                    if (fields[peça.x + i][peça.y + j] != peça.forma[i][j]) {
                        fields[peça.x + i][peça.y + j] = peça.current;
                        end = true;
                    }

                }
            }
            Random rand = new Random();
            int n = rand.nextInt(6) + 1;
            peça = new Peça(n);
        } else {

            for (int i = 0; i < peça.w; i++) {

                for (int j = 0; j < peça.h; j++) {

                    if (peça.y >= 0) {

                        if (fields[peça.x + i][peça.y + j + 1] != 0) {
                            if (fields[peça.x + i][peça.y + j] != peça.forma[i][j]) {
                                if (peça.y == 0) {
                                    gameOver = true;

                                }
                                fet = true;
                            }

                        }

                    }


                }
            }

            if (fet == false) {
                peça.y++;
            } else {
                for (int a = 0; a < peça.w; a++) {
                    // filas++;
                    for (int b = 0; b < peça.h; b++) {

                        if (fields[peça.x + a][peça.y + b] != peça.forma[a][b]) {
                            if (fields[peça.x + a][peça.y + b] == 0) {

                                fields[peça.x + a][peça.y + b] = peça.current;

                                end = true;
                            }
                        }

                    }
                }
                Random rand = new Random();
                int n = rand.nextInt(6) + 1;
                peça = new Peça(n);
                fet = false;
            }

        }

    }


    public void Borrar_Linea(int linea_a_borrar) {

        for (int a = 0; a < WORLD_WIDTH; a++) {
            fields[a][linea_a_borrar] = 0;

            if (a == 9) {

                Baixar_linea(linea_a_borrar);
                score += SCORE_INCREMENT;

            }
        }


    }

    public int[][] Rotate() {


        final int M = peça.forma.length;
        final int N = peça.forma[0].length;

        int[][] ret = new int[N][M];

        for (int r = 0; r < M; r++) {
            for (int c = 0; c < N; c++) {


                ret[c][M - 1 - r] = peça.forma[r][c];

            }

        }

        return  ret;


    }



    public boolean CanRotate()
    {

        rotable = false;
        int[][] ret = Rotate();

        int w = peça.w;
        int h = peça.h;


        if (peça.x + peça.h <= WORLD_WIDTH) {
            if (peça.y + peça.w < WORLD_HEIGHT) {

                rotable = true;
            }
        }

        if (peça.y >=0) {
            for (int j = 0; j < peça.h; j++) {

                for (int l = 0; l < peça.w; l++)

                    if (fields[peça.x + j][peça.y + l] != 0) {

                        rotable = false;
                    }
            }

        }

        return rotable;
    }


    public  void NewArray()
    {
        int[][] newArry = Rotate();

        if (CanRotate()) {
            final int M = peça.forma.length;
            final int N = peça.forma[0].length;

            peça.forma = new int[N][M];

            for (int i = 0; i < peça.forma.length; i++) {
                for (int j = 0; j < peça.forma[i].length; j++) {
                    peça.forma[i][j] = newArry[i][j];
                }
            }

            int w = peça.w;
            int h = peça.h;

            peça.w = h;
            peça.h = w;


        }

    }

    public  void Baixar_linea(int linia_baixar)
    {

        for (int i = 0; i <WORLD_WIDTH; i ++) {
            // filas++;
            for (int j = WORLD_HEIGHT-1; j> 0; j--) {

                if (j < linia_baixar +1) {
                    fields[i][j] = fields[i][j-1];

                }



            }

        }
    }
    public void Mover_derecha()
    {

        if (peça.x + peça.w != WORLD_WIDTH ) {

            canmove_der = false;

            for (int j = 0; j < peça.h; j++) {
                if (peça.y >= 0) {
                    if (fields[peça.x + peça.w][peça.y + j] != 0) {

                        canmove_der = true;
                    }


                }
            }

            if (!canmove_der)
            {
                peça.x++;
            }

        }

    }
    public void Mover_izquierda() {
        if (peça.x != 0) {

            canmove_izq = false;


            if (peça.y >= 0) {
                for (int j = 0; j < peça.h; j++) {
                    if (fields[peça.x - 1][peça.y + j] != 0) {

                        canmove_izq = true;

                    }

                }
            }


            if (!canmove_izq) {
                if (peça.x != 0) {
                    peça.x--;
                }
            }
        }
    }

    public void render(float deltaTime) {


        Graphics g = game.getGraphics();
        for (int i = 0; i <WORLD_WIDTH; i ++) {
            // filas++;
            for (int j = 0; j < WORLD_HEIGHT; j++) {

                if(fields[i][j] != 0)
                {
                    if(fields[i][j] == 1)
                    {
                        g.drawPixmap(Assets.tile_azul_flojo, i*32, j*32);
                    }
                    if(fields[i][j] == 2)
                    {
                        g.drawPixmap(Assets.tile_amarillo, i*32, j*32);
                    }
                    if(fields[i][j] == 3)
                    {
                        g.drawPixmap(Assets.tile_lila, i*32, j*32);
                    }
                    if(fields[i][j] == 4)
                    {
                        g.drawPixmap(Assets.tile_verde, i*32, j*32);
                    }
                    if(fields[i][j] == 5)
                    {
                        g.drawPixmap(Assets.tile, i*32, j*32);
                    }
                    if(fields[i][j] == 6)
                    {
                        g.drawPixmap(Assets.tile_azul, i*32, j*32);
                    }
                    if(fields[i][j] == 7)
                    {
                        g.drawPixmap(Assets.tile_naranja, i*32, j*32);
                    }

                }

            }

        }

        for (int i = 0; i < peça.w; i++) {
            // filas++;
            for (int j = 0; j < peça.h; j++) {
                if(peça.forma[i][j] != 0) {


                    g.drawPixmap(peça.Tile, (peça.x + i) * 32, (peça.y + j) * 32);


                }
            }
        }


    }








}
