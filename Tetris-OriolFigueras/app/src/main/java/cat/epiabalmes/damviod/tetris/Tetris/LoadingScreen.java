package cat.epiabalmes.damviod.tetris.Tetris;

import cat.epiabalmes.damviod.tetris.framework.Game;
import cat.epiabalmes.damviod.tetris.framework.Graphics;
import cat.epiabalmes.damviod.tetris.framework.Screen;
import cat.epiabalmes.damviod.tetris.framework.Graphics.PixmapFormat;

public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.background = g.newPixmap("background.png", PixmapFormat.RGB565);

        Assets.mainMenu = g.newPixmap("mainmenu.png", PixmapFormat.ARGB4444);
        Assets.buttons = g.newPixmap("buttons.png", PixmapFormat.ARGB4444);
        Assets.numbers = g.newPixmap("numbers.png", PixmapFormat.ARGB4444);
        Assets.tile = g.newPixmap("tile.png", PixmapFormat.ARGB4444);
        Assets.tile_amarillo = g.newPixmap("tile_amarillo.png", PixmapFormat.ARGB4444);
        Assets.tile_lila = g.newPixmap("tile_lila.png", PixmapFormat.ARGB4444);
        Assets.tile_azul = g.newPixmap("tile_azul.png", PixmapFormat.ARGB4444);
        Assets.tile_azul_flojo = g.newPixmap("tile_azul_flojo.png", PixmapFormat.ARGB4444);
        Assets.tile_verde = g.newPixmap("tile_verde.png", PixmapFormat.ARGB4444);
        Assets.tile_naranja = g.newPixmap("tile_naranja.png", PixmapFormat.ARGB4444);
        Assets.Logo = g.newPixmap("Tetris.png", PixmapFormat.ARGB4444);
        Assets.credits = g.newPixmap("Credits.png", PixmapFormat.ARGB4444);
        Assets.gameover = g.newPixmap("gameover.png", PixmapFormat.ARGB4444);
        Assets.pausa = g.newPixmap("pausa.png", PixmapFormat.ARGB4444);






        Assets.click = game.getAudio().newSound("click.ogg");
        Assets.eat = game.getAudio().newSound("eat.ogg");
        Assets.xoc = game.getAudio().newSound("bitten.ogg");
        Settings.load(game.getFileIO());

        game.setScreen(new MainMenuScreen(game));

    }
    
    public void render(float deltaTime) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void dispose() {

    }
}
