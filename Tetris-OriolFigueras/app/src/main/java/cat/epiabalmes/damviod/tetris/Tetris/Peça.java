package cat.epiabalmes.damviod.tetris.Tetris;

import cat.epiabalmes.damviod.tetris.framework.Pixmap;

public class Peça{
    int current = 1;
    int x;
    int y;
    int w;
    int h;
    int [][] forma;
    public Pixmap Tile;


    public Peça(int current) {


        this.current = current;
        x = 5;
        y = -2;

        if (current==1)
        {
            y = -4;

            w=1;
            h=4;

            forma= new int[w][h];
            forma[0][0] = current;
            forma[0][1] = current;
            forma[0][2] = current;
            forma[0][3] = current;
            Tile = Assets.tile_azul_flojo;


        }
        else if (current == 2)
        {
            w=2;
            h=2;

            forma= new int[w][h];
            forma[0][0] = current;
            forma[0][1] = current;
            forma[1][0] = current;
            forma[1][1] = current;
            Tile = Assets.tile_amarillo;

        }
        else if (current == 3)
        {
            w=3;
            h=2;
            x --;

            forma= new int[w][h];
            forma[1][0] = current;
            forma[0][1] = current;
            forma[1][1] = current;
            forma[2][1] = current;
            Tile = Assets.tile_lila;
        }

        else if (current == 4) //Z
        {
            w=3;
            h=2;

            y = -1;
            x = 2;


            forma= new int[w][h];
            forma[1][0] = current;
            forma[2][0] = current;
            forma[0][1] = current;
            forma[1][1] = current;
            Tile = Assets.tile_verde;
        }

        else if (current == 5)
        {
            w=3;
            h=2;

            x--;

            forma= new int[w][h];
            forma[0][0] = current;
            forma[1][0] = current;
            forma[2][1] = current;
            forma[1][1] = current;
            Tile = Assets.tile;
        }

        else if (current == 6)
        {
            w=3;
            h=2;

            x--;
            //y = -1;
            forma= new int[w][h];
            forma[0][0] = current;
            forma[0][1] = current;
            forma[1][1] = current;
            forma[2][1] = current;
            Tile = Assets.tile_azul;

        }

        else if (current == 7)
        {
            w=4;
            h=2;

            x--;
            forma= new int[w][h];
            forma[2][0] = current;
            forma[0][1] = current;
            forma[1][1] = current;
            forma[2][1] = current;
            Tile = Assets.tile_naranja;
        }

    }


}
