package cat.epiabalmes.damviod.tetris.Tetris;

import cat.epiabalmes.damviod.tetris.framework.Screen;
import cat.epiabalmes.damviod.tetris.framework.impl.AndroidGame;

/**
 * Created by damviod on 11/12/15.
 */
public class Tetris extends AndroidGame {
    public Screen getStartScreen() {
        return new LoadingScreen(this);
    }
}
