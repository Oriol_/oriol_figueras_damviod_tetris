package cat.epiabalmes.damviod.tetris.framework;

public interface Sound {
    public void play(float volume);

    public void dispose();
}
